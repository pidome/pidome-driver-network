/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.modules.drivers.network;

import io.vertx.core.Promise;
import java.net.InetAddress;
import java.net.URI;
import java.net.http.HttpRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.platform.hardware.driver.HardwareDriverDiscovery;
import org.pidome.platform.hardware.driver.network.AbstractNetworkDriver;
import org.pidome.platform.hardware.driver.network.NetworkConnectionProducer;
import org.pidome.platform.hardware.driver.network.NetworkDeviceConfiguration;
import org.pidome.platform.hardware.driver.network.NetworkProsumer;

/**
 * The network driver.
 *
 * @author john.sirach
 */
@HardwareDriverDiscovery(
        name = "PiDome Network driver",
        description = "Driver natively provided by PiDome."
)
@SuppressWarnings("PMD.BeanMembersShouldSerialize")
public class NetworkDriver extends AbstractNetworkDriver<NullConfiguration> implements NetworkConnectionProducer {

    /**
     * Get logger.
     */
    private static final Logger LOG = LogManager.getLogger(NetworkDriver.class);

    /**
     * The prosumer for the network connections.
     */
    private final NetworkProsumer prosumer = new NetworkProsumer();

    /**
     * Configuration set.
     */
    private NetworkDeviceConfiguration configuration;

    /**
     * Start the driver to give network access over the interface.
     *
     * @param promise Promise to fulfil when completed.
     */
    @Override
    public void startDriver(final Promise<Void> promise) {
        this.prosumer.setProducer(this);
        promise.complete();
    }

    /**
     * To stop the access over the given network interface.
     *
     * @param promise The promise to fulfil when completed.
     */
    @Override
    public void stopDriver(final Promise<Void> promise) {
        this.prosumer.setProducer(null);
        promise.complete();
    }

    /**
     * Creates the configuration for the network interface.
     *
     * @param suppliedConfiguration The base configuration to construct the
     * driver configuration from.
     * @param promise
     */
    @Override
    public void composeConfiguration(final NetworkDeviceConfiguration suppliedConfiguration, final Promise<Void> promise) {
        this.configuration = suppliedConfiguration;
        promise.complete();
    }

    /**
     * Configure the driver before the start happens with the given
     * configuration result.
     *
     * This method is ignored as for the network driver the configuration
     * composition is done by the compose configuration which is immutable
     * during the run of the driver.
     *
     * @param configurationResult The configuration result.
     * @param promise The promise to fulfil when configuring is done.
     */
    @Override
    public void configure(final NullConfiguration configurationResult, final Promise<Void> promise) {
        promise.complete();
        //ProtectionDomain peorectionDomain = new ProtectionDomain();
    }

    /**
     * The prosumer for interchanging data.
     *
     * @return A dummy prosumer until a proper solution is there.
     */
    @Override
    public NetworkProsumer getProsumer() {
        return prosumer;
    }

    /**
     * Creates a new Http client according to the user's allowed list.
     *
     * @param builder The builder to set the protocol, address and path on.
     * @param protocol The protocol to use, http/https
     * @param address The address to use in the new HttpClient.
     * @param path The path on the remote host.
     * @return A new http client with the given address.
     */
    @Override
    public HttpRequest.Builder newHttpClientBuilder(final HttpRequest.Builder builder, final String protocol, final InetAddress address, final String path) {
        final String url = protocol + "://" + address.getHostName() + ((path != null) ? path : "");
        builder.uri(URI.create(url));
        LOG.debug("URL build to return [{}]", url);
        return builder;
    }

}
