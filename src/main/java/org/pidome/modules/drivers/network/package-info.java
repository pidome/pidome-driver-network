/**
 * Driver to supply network communication.
 * <p>
 * The driver is for providing the connection means configured by the end user.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.modules.drivers.network;
